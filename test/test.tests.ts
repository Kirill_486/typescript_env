import { templateFunction } from "../src/templateFunction";
import * as assert from 'assert';

describe('Dummie test', () => {
  it('should be truthy', () => {
    const result = templateFunction();
    assert.ok(result);
  });  
});